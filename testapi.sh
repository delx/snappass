#/bin/bash

url="$1"

password="This 1s a k00l P@55w0rD!"

CURL="curl --silent"


setPassword()
{
    local passwd="$1"
    local type="$2"
    local dest_file="$3"
    $CURL --header "Content-Type: application/json" --request POST --data '{"ttl":"1day","password":"'"$passwd"'","result_type":"'"$type"'"}' $url/api/setpassword -o "$dest_file"
    
    case $type in
        json)
            jq -r .token "$dest_file"
            ;;
        text)
            cat $dest_file
            ;;
    esac
}

getPassword()
{
    local token="$1"
    local type="$2"
    local dest_file="$3"
    $CURL "$url/api/getpassword/$type/$token" -o "$dest_file"

    case $type in

        json)
            jq -r .password "$dest_file"
            ;;
        text)
            cat "$dest_file"
            ;;
    esac
}

for type in json text
do

    token=$(setPassword "$password" "json" "test_set_api.json")
    test_password=$(getPassword "$token" "$type" "test_get_api.json")

    echo -n "$type//$password// vs //$test_password//"

    if [[ "$password" == "$test_password" ]]
    then
        echo "OK"
    else
        echo "KO"
    fi

done

get_url=$(setPassword "$password" "text" "test_set_api.json")

echo $get_url

rm -f "test_set_api.json" "test_get_api.json"